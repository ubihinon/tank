package assets;

import render.Model;

/**
 * Created by ubihinon on 25.12.16.
 *
 */
public class Asset {
    private static Model model;

    public static void initAsset() {
        float[] vertices = new float[] {
                -1.0f, 1.0f, 0, // top left
                1.0f, 1.0f, 0,  // top right
                1.0f, -1.0f, 0, // bottom right
                -1.0f, -1.0f, 0, // bottom left
        };
        float[] textureArray = new float[] {
                0, 0,
                1, 0,
                1, 1,
                0, 1,
        };
        int[] indices = new int[] {
                0, 1, 2,
                2, 3, 0
        };
        model = new Model(vertices, textureArray, indices);
    }

    public static void deleteAsset() {
        model = null;
    }

    public static Model getModel() {
        return model;
    }
}
