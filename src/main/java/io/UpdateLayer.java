package io;

import assets.Asset;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import render.Camera;
import render.MainShader;
import render.Texture;

import java.awt.*;

import static org.lwjgl.glfw.GLFW.glfwSetCursorPosCallback;
import static org.lwjgl.opengl.GL11.GL_NEAREST;

/**
 * Created by ubihinon on 23.12.16.
 *
 */
public class UpdateLayer {
    private MainShader shader;
    private Window window;
    private Texture backgroundTexture;
    private Rectangle bounds = new Rectangle();
    private Vector3f position;

    public UpdateLayer(Window window) {
        glfwSetCursorPosCallback(window.getWindow(), new Mouse());
        this.window = window;
        this.shader = new MainShader();
        position = new Vector3f(0.0f, -50.0f, 0);
        addButton(345, 420);
    }

    public void render() {
        Camera camera = new Camera(window.getWidth(), window.getHeight());
        camera.setProjection(window.getWidth(), window.getHeight());
        Matrix4f target = camera.getProjection();

        Matrix4f matrix4f = new Matrix4f().setTranslation(position);

        matrix4f.scale(25);
        target.mul(matrix4f);

        shader.bind();
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", target);
        backgroundTexture.bind(0);
        Asset.getModel().render();
    }

    public void addButton(int x, int y) {
        backgroundTexture = new Texture("textures/refresh.png", true, GL_NEAREST);
        bounds.x = x;
        bounds.y = y;
        bounds.width = 60;
        bounds.height = 60;
    }

    public boolean isUpdateButtonPressed() {
        return bounds.contains(Mouse.getX(), Mouse.getY()) && Mouse.isPressedButton();
    }
}
