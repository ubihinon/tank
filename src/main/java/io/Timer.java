package io;

/**
 * Created by ubihinon on 19.11.16.
 *
 */
public class Timer {
    public static double getTime() {
        return (double) System.nanoTime() / (double) 1000000000L;
    }
}
