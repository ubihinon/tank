package io;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Created by ubihinon on 20.11.16.
 *
 */
public class Input {
    private long window;
    private boolean keys[];

    public Input(long window) {
        this.window = window;
        this.keys = new boolean[GLFW_KEY_LAST];
        for (int i = 0; i < GLFW_KEY_LAST; i++) {
            keys[i] = false;
        }
    }
//    // Setup a key callback. It will be called every time a key is pressed, repeated or released.
//    glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
//        if ( key == GLFW_KEY_ESCAPE && action == GLFW_RELEASE )
//            glfwSetWindowShouldClose(window, true); // We will detect this in our rendering loop
//    });
    public boolean isKeyDown(int key) {
        return glfwGetKey(window, key) == 1;
    }

    public boolean isKeyPressed(int key) {
        return (isKeyDown(key) && !keys[key]);
    }

    public void update() {
        for (int i = 0; i < GLFW_KEY_LAST; i++) {
            keys[i] = isKeyDown(i);
        }
    }

    public boolean isMouseButtonDown(int button) {
        return glfwGetMouseButton(window, button) == 1;
    }
}
