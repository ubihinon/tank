package io;

import org.lwjgl.glfw.GLFWCursorPosCallback;

import static org.lwjgl.glfw.GLFW.GLFW_MOUSE_BUTTON_1;
import static org.lwjgl.glfw.GLFW.glfwGetMouseButton;
import static org.lwjgl.glfw.GLFW.glfwSetMouseButtonCallback;

/**
 * Created by ubihinon on 25.12.16.
 *
 */
public class Mouse extends GLFWCursorPosCallback {
    private static double x;
    private static double y;
    private static boolean pressedButton;

    @Override
    public void invoke(long window, double xpos, double ypos) {
        pressedButton = false;
        x = xpos;
        y = ypos;
        glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_1);
        glfwSetMouseButtonCallback(window, (window1, button, action, mods) -> {
            pressedButton = true;
        });

    }

    public static double getX() {
        return x;
    }

    public static double getY() {
        return y;
    }

    static boolean isPressedButton() {
        return pressedButton;
    }
}
