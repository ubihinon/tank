package io;

import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.glfw.GLFWWindowSizeCallback;

import static org.lwjgl.glfw.Callbacks.glfwFreeCallbacks;
import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.system.MemoryUtil.NULL;

/**
 * Created by ubihinon on 19.11.16.
 *
 */
public class Window {
    private long window;
    private boolean hasResized;
    private GLFWWindowSizeCallback windowSizeCallback;

    private int width, height;
    private boolean fullScreen;
    private Input input;

    public Window() {
        setFullScreen(false);
        hasResized = false;
    }

    private void setLocalCallbacks() {
        windowSizeCallback = new GLFWWindowSizeCallback() {
            @Override
            public void invoke(long argWindow, int argWidth, int argHeight) {
                width = argWidth;
                height = argHeight;
                hasResized = true;
            }
        };

        glfwSetWindowSizeCallback(window, windowSizeCallback);
    }

    public void createWindow(String title) {
        window = glfwCreateWindow(width, height, title, fullScreen ? glfwGetPrimaryMonitor() : 0, 0);
        if ( window == NULL )
            throw new RuntimeException("Failed to create the GLFW window");

        glfwSetKeyCallback(window, (window, key, scancode, action, mods) -> {
            if (input.isKeyDown(GLFW_KEY_ESCAPE))
                glfwSetWindowShouldClose(window, true); // We will detect this in our rendering loop
        });

        if(!fullScreen) {
            // Get the resolution of the primary monitor
            GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
            // Center our window
            glfwSetWindowPos(
                    window,
                    (vidmode.width() - width) / 2,
                    (vidmode.height() - height) / 2
            );
            glfwShowWindow(window);
        }
        // Make the OpenGL context current
        glfwMakeContextCurrent(window);
        // Enable v-sync
        glfwSwapInterval(1);
        // Make the window visible
//        glfwShowWindow(window);
        input = new Input(window);

        setLocalCallbacks();
    }

    public void cleanUp() {
        windowSizeCallback.close();
    }

    public boolean shouldClose() {
        return glfwWindowShouldClose(window);
    }

    public void swapBuffers() {
        // swap the color buffers
        glfwSwapBuffers(window);
    }

    public void destroy() {
        // Free the window callbacks and destroy the window
        glfwFreeCallbacks(window);
        glfwDestroyWindow(window);
        System.exit(0);
    }

    public void setSize(int width, int height) {
        this.width  = width;
        this.height = height;
    }

    public static void setCallbacks() {
        glfwSetErrorCallback(new GLFWErrorCallback() {
            @Override
            public void invoke(int error, long description) {
                throw new IllegalStateException(GLFWErrorCallback.getDescription(description));
            }
        });
    }

    public void update() {
        hasResized = false;
        glfwPollEvents();
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public boolean isFullScreen() {
        return fullScreen;
    }

    public void setFullScreen(boolean fullScreen) {
        this.fullScreen = fullScreen;
    }

    public long getWindow() {
        return window;
    }

    public Input getInput() {
        return input;
    }

    public boolean isHasResized() {
        return hasResized;
    }
}
