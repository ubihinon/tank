package fontCreator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.lang.*;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by ubihinon on 16.12.16.
 *
 */
public class MetaFile {
    protected static final double LINE_HEIGHT = 0.03f;
    public static final int SPACE_ASCII = 32;

    private static final int PAD_TOP = 0;
    private static final int PAD_LEFT = 1;
    private static final int PAD_BOTTOM = 2;
    private static final int PAD_RIGHT = 3;

    private static final String SPLITTER = " ";
    private static final String NUMBER_SEPARATOR = ",";

    private double aspectRatio;

    private double verticalPerPixelSize;
    private double horizontalPerPixelSize;
    private double spaceWidth;
    private int[] padding;
    private int paddingWidth;
    private int paddingHeight;


    private Map<Integer, Character> metaData = new HashMap<>();
    private BufferedReader reader;
    private Map<String, String> values = new HashMap<>();

    public MetaFile(File file) {
        try {
            openFile(file);
            loadPaddingData();
            loadLineSizes();
//        int imageWidth = getValueOfVariable("scaleW");
            int imageWidth = 512;
            loadCharacterData(imageWidth);
            close();
        } catch (NumberFormatException ignored) {
        }
    }

    /**
     * Opens the font file, ready for reading.
     *
     * @param file
     *            - the font file.
     */
    private void openFile(File file) {
        try {
            reader = new BufferedReader(new FileReader(file));
        } catch (Exception e) {
            e.printStackTrace();
            System.err.println("Couldn't read font meta file!");
        }
    }

    /**
     * Closes the font file after finishing reading.
     */
    private void close() {
        try {
            reader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Read in the next line and store the variable values.
     *
     * @return {@code true} if the end of the file hasn't been reached.
     */
    private boolean processNextLine() {
        values.clear();
        String line = null;
        try {
            line = reader.readLine();
        } catch (IOException e1) {
        }
        if (line == null) {
            return false;
        }
        for (String part : line.split(SPLITTER)) {
            String[] valuePairs = part.split("=");
            if (valuePairs.length == 2) {
                values.put(valuePairs[0], valuePairs[1]);
            }
        }
        return true;
    }

    /**
     * Loads the data about how much padding is used around each character in
     * the texture atlas.
     */
    private void loadPaddingData() {
        processNextLine();
        this.padding = getValuesOfVariable("padding");
        this.paddingWidth = padding[PAD_LEFT] + padding[PAD_RIGHT];
        this.paddingHeight = padding[PAD_TOP] + padding[PAD_BOTTOM];
    }

    /**
     * Gets the array of ints associated with a variable on the current line.
     *
     * @param variable
     *            - the name of the variable.
     * @return The int array of values associated with the variable.
     */
    private int[] getValuesOfVariable(String variable) {
        String[] numbers = values.get(variable).split(NUMBER_SEPARATOR);
        int[] actualValues = new int[numbers.length];
        for (int i = 0; i < actualValues.length; i++) {
            actualValues[i] = Integer.parseInt(numbers[i]);
        }
        return actualValues;
    }

    /**
     * Loads information about the line height for this font in pixels, and uses
     * this as a way to find the conversion rate between pixels in the texture
     * atlas and screen-space.
     */
    private void loadLineSizes() {
        processNextLine();
        int lineHeightPixels = getValueOfVariable("lineHeight") - paddingHeight;
        verticalPerPixelSize = LINE_HEIGHT / (double) lineHeightPixels;
        horizontalPerPixelSize = verticalPerPixelSize / aspectRatio;
    }

    /**
     * Gets the {@code int} value of the variable with a certain name on the
     * current line.
     *
     * @param variable
     *            - the name of the variable.
     * @return The value of the variable.
     */
    private int getValueOfVariable(String variable) throws NumberFormatException {
        return Integer.parseInt(values.get(variable));
    }

    /**
     * Loads in data about each character and stores the data in the
     * {@link java.lang.Character} class.
     *
     * @param imageWidth
     *            - the width of the texture atlas in pixels.
     */
    private void loadCharacterData(int imageWidth) throws NumberFormatException {
        processNextLine();
        processNextLine();
        while (processNextLine()) {
            Character c = loadCharacter(imageWidth);
            if (c != null) {
                metaData.put(c.getId(), c);
            }
        }
    }

    private Character loadCharacter(int imageSize) throws NumberFormatException {
        int id          = getValueOfVariable("id");
        if(id == SPACE_ASCII) {
            spaceWidth = 3;
            return null;
        }
        double xTex     = getValueOfVariable("x");
        double yTex     = getValueOfVariable("y");
        int width       = getValueOfVariable("width");
        int height      = getValueOfVariable("height");
        double xOffset  = getValueOfVariable("xoffset");
        double yOffset  = getValueOfVariable("yoffset");
        double xAdvance = getValueOfVariable("xadvance");
        double xTexSize = (double) width / imageSize;
        double yTexSize = (double) height / imageSize;
        return new Character(id, xTex, yTex, xTexSize, yTexSize,
                xOffset, yOffset, width, height, xAdvance);
    }

    public Map<Integer, Character> getMetaData() {
        return metaData;
    }
}
