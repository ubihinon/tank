package fontCreator;

import assets.Asset;
import io.Window;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import render.Camera;
import render.MainShader;
import render.Texture;

import java.io.IOException;
import java.util.Map;
import java.util.concurrent.*;

import static fontCreator.MetaFile.SPACE_ASCII;
import static org.lwjgl.opengl.GL11.*;

/**
 * Created by ubihinon on 16.12.16.
 *
 */
public class Text {
    private MainShader shader;
    private float xCoord;
    private float scale = 10;
    private float x;
    private float y;
    private Window window;
    private TextCreator textCreator;

    public Text(Window window) {
        this.window = window;
        this.shader = new MainShader();
        this.textCreator = new TextCreator();
    }

    public void render(Texture characterTexture) {
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);

        Camera camera = new Camera(window.getWidth(), window.getHeight());
        camera.setProjection(window.getWidth(), window.getHeight());
        Matrix4f target = camera.getProjection();
        Matrix4f matrix4f = new Matrix4f().setTranslation(new Vector3f(x, y, 0));

        matrix4f.translate(0 + xCoord, 45.5f, 0.0f);
        matrix4f.scale(scale);
        target.mul(matrix4f);

        shader.bind();
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", target);
        characterTexture.bind(0);
        Asset.getModel().render();

        glDisable(GL_BLEND);
    }

    public void draw(String text, float scale, float x, float y) {
        this.scale = scale;
        this.x = x;
        this.y = y;
        try {
            xCoord = 0;
            textCreator.setText(text);

            for(int i = 0; i < text.length(); i++) {
                if(text.codePointAt(i) != SPACE_ASCII) {
                    render(TextCreator.getCachedCharacters().get(text.codePointAt(i)));
                    xCoord += 1.8 * scale;
                } else {
                    xCoord += scale;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}