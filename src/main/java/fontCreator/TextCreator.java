package fontCreator;

import render.Texture;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import static org.lwjgl.opengl.GL11.GL_LINEAR;

/**
 * Created by ubihinon on 16.12.16.
 *
 */
public class TextCreator {
    private Map<Integer, Character> metaData = new HashMap<>();
    private String text;
    private BufferedImage charactersImage;
    private static Map<Integer, Texture> cachedCharacters = new HashMap<>();

    public TextCreator() {
    }

    public void init() {
        try {
            MetaFile metaFile = new MetaFile(
                    new File(System.getProperty("user.dir") + "/fonts/Arial.fnt")
            );
            metaData.putAll(metaFile.getMetaData());
            charactersImage = ImageIO.read(
                    new File(System.getProperty("user.dir") + "/fonts/Arial.png")
            );

            metaData.forEach((integer, character) -> {
                try {
                    getImageCharacter(character);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void getImageCharacter(Character character) throws IOException {
        int w = (int) character.getSizeX();
        int h = (int) character.getSizeY();
        if(w <= 0 || h <= 0) {
            return;
        }
        int size = (w > h) ? w : h;

        BufferedImage characterImage = new BufferedImage(size, size,
                BufferedImage.TYPE_INT_ARGB);
        characterImage.getGraphics().drawImage(charactersImage, 0, 0, w, h,
                (int) character.getxTextureCoord(),
                (int) character.getyTextureCoord(),
                (int) character.getxTextureCoord() + w,
                (int) character.getyTextureCoord() + h,
                null);
        ImageIO.write(characterImage, "png", new File("currentCharacter.png"));

        cachedCharacters.put(
                character.getId(),
                new Texture("currentCharacter.png", false, GL_LINEAR)
        );
    }

    public void setText(String text) {
        this.text = text;
    }

    public String getText() {
        return text;
    }

    static Map<Integer, Texture> getCachedCharacters() {
        return cachedCharacters;
    }
}
