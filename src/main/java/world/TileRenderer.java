package world;

import assets.Asset;
import org.joml.Matrix4f;
import org.joml.Vector3f;
import render.Camera;
import render.MainShader;
import render.Texture;

import java.util.HashMap;

import static org.lwjgl.opengl.GL11.GL_NEAREST;

/**
 * Created by ubihinon on 21.11.16.
 *
 */
public class TileRenderer {
    private HashMap<String, Texture> tileTextures;

    public TileRenderer() {
        tileTextures = new HashMap<>();

        for (int i = 0; i < Tile.tiles.length; i++) {
            if(Tile.tiles[i] != null) {
                if(!tileTextures.containsKey(Tile.tiles[i].getTexture())) {
                    String tex = Tile.tiles[i].getTexture();
                    tileTextures.put(tex, new Texture("textures/" +tex + ".png", true, GL_NEAREST));
                }
            }
        }
    }

    public void renderTile(Tile tile, int x, int y, MainShader shader, Matrix4f world, Camera camera) {
        shader.bind();
        if(tileTextures.containsKey(tile.getTexture())) {
            tileTextures.get(tile.getTexture()).bind(0);
        }
        Matrix4f tilePos = new Matrix4f().translate(new Vector3f(x * 2, y * 2, 0));
        Matrix4f target = new Matrix4f();

        camera.getProjection().mul(world, target);
        target.mul(tilePos);

        shader.setUniform("sampler", 0);
        shader.setUniform("projection", target);

        Asset.getModel().render();
    }
}
