package world;

import collision.AABB;
import entity.*;
import fontCreator.Text;
import io.Window;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import render.Camera;
import render.MainShader;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * Created by ubihinon on 22.11.16.
 *
 */
public class World {
    private int viewX;
    private int viewY;
    private byte[] tiles;
    private AABB[] boundingBoxes;
    private CopyOnWriteArrayList<Entity> entities;
    private int width;
    private int height;
    private Matrix4f world;
    private int scale;
    private Text text;
    private boolean canCreateEnemy = false;
    private Transform enemyTransform = null;
    private Transform playerTransform = null;
    private int countEnemies = 0;
    private final int COUNT_ENEMIES = 10;
    private final int CURRENT_COUNT_ENEMIES = 4;
    private int tries = 2;
    private Window window;
    private AtomicInteger killedEnemies = new AtomicInteger();

    public World(String world, Camera camera) {
        try {
            BufferedImage tileSheet = ImageIO.read(
                    new File(System.getProperty("user.dir") + "/textures/levels/" + world + "/tiles.png")
            );
            BufferedImage entitySheet = ImageIO.read(
                    new File(System.getProperty("user.dir") + "/textures/levels/" + world + "/entities.png")
            );

            width = tileSheet.getWidth();
            height = tileSheet.getHeight();

            scale = 25;
            this.world = new Matrix4f().setTranslation(new Vector3f(0));
            this.world.scale(scale);

            int[] colorTileSheet = tileSheet.getRGB(0, 0, width, height, null, 0, width);
            int[] colorEntitySheet = entitySheet.getRGB(0, 0, width, height, null, 0, width);

            tiles = new byte[width * height];
            boundingBoxes = new AABB[width * height];
            entities = new CopyOnWriteArrayList<>();

            Transform transform;

            for (int y = 0; y < height; y++) {
                for (int x = 0; x < width; x++) {
                    int red = (colorTileSheet[x + y * width] >> 16) & 0xFF;
                    int playerIndex = (colorEntitySheet[x + y * width] >> 8) & 0xFF;
                    int enemyIndex = (colorEntitySheet[x + y * width] >> 16) & 0xFF;
                    int entityAlpha = (colorEntitySheet[x + y * width] >> 24) & 0xFF;

                    Tile tile;
                    try {
                        tile = Tile.tiles[red];
                    } catch (ArrayIndexOutOfBoundsException e) {
                        tile = null;
                    }

                    if (tile != null) {
                        setTile(tile, x, y);
                    }
                    if (entityAlpha > 0) {
                        transform = new Transform();
                        transform.pos.x = x * 2;
                        transform.pos.y = -y * 2;

                        if (playerIndex == 1) {
                            playerTransform = transform.copy();
                            Player player = new Player(playerTransform.copy());
                            entities.add(player);
                            camera.getPosition().set(transform.pos.mul(-scale, new Vector3f()));
                        }
                        if (enemyIndex == 1) {
                            enemyTransform = transform.copy();
                            new Timer().schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    final int[] count = {0};
                                    entities.forEach(entity -> {
                                        if(entity instanceof Enemy) {
                                            count[0]++;
                                        }
                                    });
                                    if(countEnemies >= COUNT_ENEMIES) {
                                        cancel();
                                        return;
                                    }
                                    if(count[0] < CURRENT_COUNT_ENEMIES) {
                                        canCreateEnemy = true;
                                        countEnemies++;
                                    }
                                }
                            }, 0, 3000);
                        }
                    }
                }
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void calculateView(Window window) {
        scale = (window.getWidth() > 800) ? 40 : 25;
        this.world = new Matrix4f().setTranslation(new Vector3f(0));
        this.world.scale(scale);

        viewX = (window.getWidth() / (scale * 2)) + 4;
        viewY = (window.getHeight() / (scale * 2)) + 4;

        this.window = window;
        text = new Text(window);
    }

    public void render(TileRenderer render, MainShader shader, Camera camera) {
        createPlayer();

        if(canCreateEnemy && enemyTransform != null) {
            Enemy enemy = new Enemy(enemyTransform.copy());
            entities.add(enemy);
            canCreateEnemy = false;
        }

        int posX = (int) camera.getPosition().x / (scale * 2);
        int posY = (int) camera.getPosition().y / (scale * 2);
        for (int i = 0; i < viewX; i++) {
            for (int j = 0; j < viewY; j++) {
                Tile tile = getTile(i - posX - (viewX / 2) + 1, j + posY - (viewY / 2));
                if(tile != null) {
                    render.renderTile(tile,
                            i - posX - (viewX / 2) + 1,
                            -j - posY + (viewY / 2),
                            shader,
                            world,
                            camera);
                }
            }
        }

        for(int i = 0; i < entities.size(); i++) {
            if(entities.get(i) instanceof Tank) {
                if(entities.get(i).isDestroyed()) {
                    entities.remove(entities.get(i));
                }
            }
        }

        text.draw("HEALTH: " + String.valueOf(tries),
                14,
                -window.getWidth() / 2.5f,
                window.getHeight() / 2 - 75);

        text.draw("KILLED: " + String.valueOf(killedEnemies.get() + "/" + COUNT_ENEMIES),
                14,
                -window.getWidth() / 6 + 100,
                window.getHeight() / 2 - 75
        );

        for (Entity entity : entities) {
            entity.render(shader, camera, this);
        }
    }

    public void update(float delta, Window window, Camera camera) {
        for(Entity entity : entities) {
            entity.update(delta, window, camera, this);
        }

        for (int i = 0; i < entities.size(); i++) {

            entities.get(i).collideWithTiles(this);

            for (int j = i + 1; j < entities.size(); j++) {

                if(entities.get(i) instanceof Tank) {
                    int finalJ = j;
                    int finalI = i;

                    ((Tank) entities.get(i)).getBullets().forEach(bullet -> {
                        bullet.collideWithTiles(this);

                        if(oneIsEnemy(finalI, finalJ)) {
                            bullet.collideWithEntity(entities.get(finalJ));

                            if(entities.get(finalI) instanceof Player
                                    && entities.get(finalJ) instanceof Enemy
                                    && bullet.isBlowingUp()) {

                                killedEnemies.incrementAndGet();
                                bullet.setBlowingUp(false);
                            }
                        }
                    });

                    ((Tank) entities.get(j)).getBullets().forEach(bullet -> {
                        bullet.collideWithTiles(this);
                        if(oneIsEnemy(finalI, finalJ)) {
                            bullet.collideWithEntity(entities.get(finalI));
                        }
                    });
                }
                entities.get(i).collideWithEntity(entities.get(j));
            }
            entities.get(i).collideWithTiles(this);
        }
    }

    private void createPlayer() {
        final boolean[] playerIsExists = {false};
        entities.forEach(entity -> {
            if(entity instanceof Player) {
                playerIsExists[0] = true;
            }
        });

        if(!playerIsExists[0]) {
            tries--;
            Player player = new Player(playerTransform.copy());
            entities.add(player);
        }
    }

    private boolean oneIsEnemy(int i, int j) {
        return !(entities.get(i) instanceof Enemy) || !(entities.get(j) instanceof Enemy);
    }

    public void correctCamera(Camera camera, Window window) {
        Vector3f pos = camera.getPosition();
        int w = -width * scale * 2;
        int h = height * scale * 2;

        if(pos.x > -(window.getWidth() / 2) + scale) {
            pos.x = -(window.getWidth() / 2) + scale;
        }
        if(pos.x < w + (window.getWidth() / 2) + scale) {
            pos.x = w + (window.getWidth() / 2) + scale;
        }
        if(pos.y < (window.getHeight() / 2) - scale) {
            pos.y = (window.getHeight() / 2) - scale;
        }
        if(pos.y > h - (window.getHeight() / 2) - scale) {
            pos.y = h - (window.getHeight() / 2) - scale;
        }
    }

    private void setTile(Tile tile, int x, int y) {
        tiles[x + y * width] = tile.getId();

        if(tile.isSolid()) {
            boundingBoxes[x + y * width] =
                    new AABB(new Vector2f(x * 2, -y * 2), new Vector2f(1.1f, 1.1f));
        } else {
            boundingBoxes[x + y * width] = null;
        }
    }

    public Tile getTile(int x, int y) {
        try {
            return Tile.tiles[tiles[x + y * width]];
        } catch(ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    public AABB getTileBoundingBox(int x, int y) {
        try {
            return boundingBoxes[x + y * width];
        } catch(ArrayIndexOutOfBoundsException e) {
            return null;
        }
    }

    public int getScale() {
        return scale;
    }

    public Matrix4f getWorldMatrix() {
        return world;
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getTries() {
        return tries;
    }

    public void setTries(int tries) {
        this.tries = tries;
    }

    public AtomicInteger getKilledEnemies() {
        return killedEnemies;
    }

    public int getCOUNT_ENEMIES() {
        return COUNT_ENEMIES;
    }
}