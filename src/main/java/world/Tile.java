package world;

/**
 * Created by ubihinon on 21.11.16.
 *
 */
public class Tile {
    private byte id;
    private String texture;
    private boolean solid;
    public static Tile tiles[] = new Tile[255];
    public static byte numberOfTile = 0;
    public static final Tile grass = new Tile("grass");
    public static final Tile wall = new Tile("wall").setSolid();
    private boolean destroyed;

    public Tile(String texture) {
        this.id = numberOfTile;
        numberOfTile++;
        this.texture = texture;
        this.solid = false;
        if(tiles[id] != null) {
            throw new IllegalStateException("Tile at [" + id + "] is already being used!");
        }
        tiles[id] = this;
        destroyed = false;
    }

    public byte getId() {
        return id;
    }

    public void setId(byte id) {
        this.id = id;
    }

    public String getTexture() {
        return texture;
    }

    public void setTexture(String texture) {
        this.texture = texture;
    }

    public Tile setSolid() {
        this.solid = true;
        return this;
    }

    public boolean isSolid() {
        return solid;
    }

    public void setDestroyed(boolean destroyed) {
        this.destroyed = destroyed;
    }

    public boolean isDestroyed() {
        return destroyed;
    }
}
