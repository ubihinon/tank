package collision;

import org.joml.Vector2f;

/**
 * Created by ubihinon on 23.11.16.
 *
 */
public class Collision {
    public Vector2f distance;
    public boolean isIntersecting;

    Collision(Vector2f distance, boolean isIntersecting) {
        this.distance = distance;
        this.isIntersecting = isIntersecting;
    }
}
