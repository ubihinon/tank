package collision;

import org.joml.Math;
import org.joml.Vector2f;

/**
 * Created by ubihinon on 23.11.16.
 * This class correct position when collision happened
 */
public class AABB {
    private Vector2f center, halfExtent;
    private int currentDirection;
    private static final int TOP    = 0;
    private static final int DOWN   = 1;
    private static final int LEFT   = 2;
    private static final int RIGHT  = 3;

    public AABB(Vector2f center, Vector2f halfExtent) {
        this.center = center;
        this.halfExtent = halfExtent;
    }

    public Collision getCollision(AABB box2) {
        Vector2f distance = box2.center.sub(center, new Vector2f());
        distance.x = Math.abs(distance.x);
        distance.y = Math.abs(distance.y);

        distance.sub(halfExtent.add(box2.halfExtent, new Vector2f()));

        return new Collision(distance, distance.x < 0 && distance.y < 0);
    }

    public void correctPosition(AABB box2, Collision data) {
        Vector2f correction = box2.center.sub(center, new Vector2f());
        if(data.distance.x > data.distance.y) {
            if(correction.x > 0) {
                center.add(data.distance.x, 0);
            } else {
                center.add(-data.distance.x, 0);
            }
        } else {
            if(correction.y > 0) {
                center.add(0, data.distance.y);
            } else {
                center.add(0, -data.distance.y);
            }
        }
    }

    public void correctPosition(Collision data) {
        if(currentDirection == TOP) {
            center.add(0, data.distance.y);
        } else if(currentDirection == DOWN) {
            center.add(0, -data.distance.y);
        } else if(currentDirection == LEFT) {
            center.add(-data.distance.x, 0);
        } else if(currentDirection == RIGHT) {
            center.add(data.distance.x, 0);
        }
    }

    public Vector2f getCenter() {
        return center;
    }

    public void setCurrentDirection(int currentDirection) {
        this.currentDirection = currentDirection;
    }
}
