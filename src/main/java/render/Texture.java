package render;

import org.lwjgl.BufferUtils;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.nio.ByteBuffer;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.glActiveTexture;

/**
 * Created by ubihinon on 17.11.16.
 *
 */
public class Texture {
    private int id;
    private int width;
    private int height;

    public Texture(String filename, boolean fromRecourse, int interpolation) {
        BufferedImage bufferedImage;
        try {
            if(fromRecourse) {
                bufferedImage = ImageIO.read(
                        new File(System.getProperty("user.dir") + "/" + filename)
                );
            } else {
                bufferedImage = ImageIO.read(
                        new File(System.getProperty("user.dir") + "/" + filename)
                );
            }

            createTexture(bufferedImage, interpolation);
        } catch (Exception ignored) {
        }
    }

    public Texture(BufferedImage buffered, int interpolation) {
        try {
            createTexture(buffered, interpolation);
        } catch (Exception ignored) {
        }
    }

    private void createTexture(BufferedImage bufferedImage, int interpolation) {
        width = bufferedImage.getWidth();
        height = bufferedImage.getHeight();

        int[] pixelsRaw = bufferedImage.getRGB(0, 0, width, height, null, 0, width);

        ByteBuffer pixels = BufferUtils.createByteBuffer(width * height * 4);

        for(int i = 0; i < width; i++) {
            for (int j = 0; j < height; j++) {
                int pixel = pixelsRaw[i * width + j];
                pixels.put((byte) ((pixel >> 16) & 0xFF));  // red
                pixels.put((byte) ((pixel >> 8) & 0xFF));   // green
                pixels.put((byte) (pixel & 0xFF));          // blue
                pixels.put((byte) ((pixel >> 24) & 0xFF));  // alpha
            }
        }

        pixels.flip();
        id = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, id);

        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, interpolation);
        glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, interpolation);

        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, pixels);
    }

    protected void finalize() throws Throwable {
        glDeleteTextures(id);
        super.finalize();
    }

    public void bind(int sampler) {
        if(sampler >= 0 && sampler <= 31) {
            glActiveTexture(GL_TEXTURE + sampler);
            glBindTexture(GL_TEXTURE_2D, id);
        }
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }
}
