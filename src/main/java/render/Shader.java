package render;

import org.joml.Matrix4f;
import org.lwjgl.BufferUtils;
import org.lwjgl.opengl.GL20;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.nio.FloatBuffer;

import static org.lwjgl.opengl.GL20.*;

/**
 * Created by ubihinon on 23.12.16.
 *
 */
abstract public class Shader {
    private int programId;
    private int vertexShaderId;
    private int fragmentShaderId;

    public Shader(String filename) {
        programId = glCreateProgram();

        vertexShaderId = glCreateShader(GL_VERTEX_SHADER);
        glShaderSource(vertexShaderId, readFile(filename + ".vs"));
        glCompileShader(vertexShaderId);
        if(glGetShaderi(vertexShaderId, GL_COMPILE_STATUS) != 1) {
            System.err.println(glGetShaderInfoLog(vertexShaderId));
            System.exit(1);
        }

        fragmentShaderId = glCreateShader(GL_FRAGMENT_SHADER);
        glShaderSource(fragmentShaderId, readFile(filename + ".fs"));
        glCompileShader(fragmentShaderId);
        if(glGetShaderi(fragmentShaderId, GL_COMPILE_STATUS) != 1) {
            System.err.println(glGetShaderInfoLog(fragmentShaderId));
            System.exit(1);
        }

        glAttachShader(programId, vertexShaderId);
        glAttachShader(programId, fragmentShaderId);

        bindAttributes();

        glLinkProgram(programId);
        if(glGetProgrami(programId, GL_LINK_STATUS) != 1) {
            System.err.println(glGetProgramInfoLog(programId));
            System.exit(1);
        }
        glValidateProgram(programId);
        if(glGetProgrami(programId, GL_VALIDATE_STATUS) != 1) {
            System.err.println(glGetProgramInfoLog(programId));
            System.exit(1);
        }
    }

    protected abstract void bindAttributes();

    protected void bindAttribute(int attribute, String variableName){
        GL20.glBindAttribLocation(programId, attribute, variableName);
    }

    public void setUniform(String name, int value) {
        int location = glGetUniformLocation(programId, name);
        if(location != -1) {
            glUniform1i(location, value);
        }
    }

    public void setUniform(String name, Matrix4f value) {
        int location = glGetUniformLocation(programId, name);
        FloatBuffer buffer = BufferUtils.createFloatBuffer(16);
        value.get(buffer);
        if(location != -1) {
            glUniformMatrix4fv(location, false, buffer);
        }
    }

    public void bind() {
        glUseProgram(programId);
    }

    private String readFile(String filename) {
        StringBuilder stringBuilder = new StringBuilder();
        BufferedReader bufferedReader;
        try {
            bufferedReader = new BufferedReader(
                    new FileReader(
                            new File(System.getProperty("user.dir") + "/shaders/" + filename)
                    )
            );

            String line;
            while ((line = bufferedReader.readLine()) != null) {
                stringBuilder.append(line);
                stringBuilder.append("\n");
            }
            bufferedReader.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return stringBuilder.toString();
    }

    protected void finalize() throws Throwable {
        super.finalize();
        glDetachShader(programId, vertexShaderId);
        glDetachShader(programId, fragmentShaderId);
        glDeleteShader(vertexShaderId);
        glDeleteShader(fragmentShaderId);
        glDeleteProgram(programId);
    }
}
