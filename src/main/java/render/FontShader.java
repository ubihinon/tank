package render;

/**
 * Created by ubihinon on 23.12.16.
 *
 */
public class FontShader extends Shader {

    private int location_colour;
    private int location_translation;

    public FontShader(String filename) {
        super(filename);
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "position");
        super.bindAttribute(1, "textureCoords");
    }
}
