package render;

import io.Timer;

import java.util.ArrayList;
import java.util.HashMap;

import static org.lwjgl.opengl.GL11.GL_NEAREST;

/**
 * Created by ubihinon on 23.11.16.
 *
 */
public class Animation {
    private ArrayList<Texture> frames;
    private int pointer;
    private double elapsedTime;
    private double currentTime;
    private double lastTime;
    private double fps;
    private static HashMap<String, ArrayList<Texture>> cachedAnimations = new HashMap<>();

    public Animation(int amount, int fps, String filename, int xGrid, int yGrid, int tileSize) {
        this.frames = new ArrayList<>(amount);
        int x = xGrid;

        String key = filename + x + yGrid + tileSize;
        if(cachedAnimations.containsKey(key)) {
            this.frames.addAll(cachedAnimations.get(key));
        } else {
            SpriteSheet spriteSheet = new SpriteSheet();
            init(fps);

            for (int i = xGrid; i < xGrid + amount; i++, x++) {
                this.frames.add(new Texture(
                                spriteSheet.getSprite(filename, x, yGrid, tileSize), GL_NEAREST
                        )
                );
            }
            cachedAnimations.put(key, this.frames);
        }
    }

    public Animation(int amountX, int amountY, int fps, String filename, int xGrid, int yGrid, int tileSize) {

        this.frames = new ArrayList<>(amountX * amountY);
        int x = xGrid;
        int y = yGrid;
        String key = filename + x + y + xGrid + yGrid + tileSize;
        if(cachedAnimations.containsKey(key)) {
            this.frames.addAll(cachedAnimations.get(key));
        } else {
            SpriteSheet spriteSheet = new SpriteSheet();
            init(fps);

            for (int i = xGrid; i < xGrid + amountX; i++, x++) {
                for (int j = yGrid; j < yGrid + amountY; j++, y++) {
                    this.frames.add(new Texture(
                                    spriteSheet.getSprite(filename, x, y, tileSize) , GL_NEAREST
                            )
                    );

                }
            }
            cachedAnimations.put(key, this.frames);
        }
    }

    private void init(int fps) {
        this.pointer = 0;
        this.elapsedTime = 0;
        this.currentTime = 0;
        this.lastTime = Timer.getTime();
        this.fps = 1.0 / (double) fps;
    }

    public void bind() {
        bind(0);
    }

    public void bind(int sampler) {
        currentTime = Timer.getTime();
        elapsedTime += currentTime - lastTime;

        if(elapsedTime >= fps) {
            elapsedTime = 0;
            pointer++;
        }

        if(pointer >= frames.size()) {
            pointer = 0;
        }
        lastTime = currentTime;
        frames.get(pointer).bind(sampler);
    }

    public Animation setFps(double fps) {
        this.fps = 1.0 / fps;
        return this;
    }
}
