package render;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.concurrent.Callable;

/**
 * Created by ubihinon on 05.12.16.
 * This class get sub image from image
 */

public class SpriteSheet {

    private static BufferedImage spriteSheet;

    public BufferedImage getSprite(String path, int xGrid, int yGrid, int tileSize) {
        try {
            spriteSheet = ImageIO.read(new File(System.getProperty("user.dir") + "/" + path));
        } catch (IOException e) {
            e.printStackTrace();
        }
        return spriteSheet.getSubimage(xGrid * tileSize, yGrid * tileSize, tileSize, tileSize);
    }
}
