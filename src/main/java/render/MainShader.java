package render;

/**
 * Created by ubihinon on 17.11.16.
 *
 */
public class MainShader extends Shader {

    public MainShader() {
        super("mainShader");
    }

    @Override
    protected void bindAttributes() {
        super.bindAttribute(0, "vertices");
        super.bindAttribute(1, "textures");
    }
}
