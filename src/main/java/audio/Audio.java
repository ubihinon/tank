package audio;

import org.apache.commons.io.IOUtils;
import org.lwjgl.BufferUtils;
import org.lwjgl.openal.*;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.UnsupportedAudioFileException;
import java.io.IOException;
import java.nio.ByteBuffer;
import java.nio.IntBuffer;

import static org.lwjgl.openal.ALC10.*;
import static org.lwjgl.openal.EXTEfx.ALC_MAX_AUXILIARY_SENDS;

/**
 * Created by ubihinon on 26.12.16.
 *
 */
public class Audio {
    private static long device;
    private static long time;
    private static Source source;

    public static int loadSound(String file) {
        IntBuffer buffer = null;
        try {
            buffer = BufferUtils.createIntBuffer(1);
            time = createBufferData(buffer, file);
        } catch (Exception e) {
            e.printStackTrace();
        }
        assert buffer != null;
        return buffer.get(0);
    }

    public static void init() throws Exception {
        device = ALC10.alcOpenDevice((ByteBuffer) null);

        ALCCapabilities deviceCaps = ALC.createCapabilities(device);
        IntBuffer contextAttributeList = BufferUtils.createIntBuffer(16);

        contextAttributeList.put(ALC_REFRESH);
        contextAttributeList.put(60);

        contextAttributeList.put(ALC_SYNC);
        contextAttributeList.put(ALC_FALSE);

        contextAttributeList.put(ALC_MAX_AUXILIARY_SENDS);
        contextAttributeList.put(2);

        contextAttributeList.put(0);
        contextAttributeList.flip();

        long newContext = ALC10.alcCreateContext(device, contextAttributeList);

        if(!ALC10.alcMakeContextCurrent(newContext)) {
            throw new Exception("Failed to make context current");
        }

        AL.createCapabilities(deviceCaps);
    }

    public static void cleanUp() {
        alcCloseDevice(device);
    }

    public static void setListenerData(float x, float y) {
        AL10.alListener3f(AL10.AL_VELOCITY, x, y, 0f);
        AL10.alListener3f(AL10.AL_ORIENTATION, 0f, 0f, -1f);
    }

    private static long createBufferData(IntBuffer buffer, String file)
            throws IOException, UnsupportedAudioFileException {
        final int MONO = 1, STEREO = 2;

        AL10.alGenBuffers(buffer);

        AudioInputStream stream = AudioSystem.getAudioInputStream(
                Audio.class.getClassLoader().getResource(file));

        AudioFormat format = stream.getFormat();
        if(format.isBigEndian()) {
            throw new UnsupportedAudioFileException("Can't handle big endian formats yet");
        }

        //load stream into byte buffer
        int openAlFormat = -1;
        if(format.getChannels() == MONO || format.getChannels() == STEREO) {
            switch (format.getSampleSizeInBits()) {
                case 8:
                    openAlFormat = AL10.AL_FORMAT_MONO8;
                    break;
                case 16:
                    openAlFormat = AL10.AL_FORMAT_MONO16;
                    break;
            }
        }

        // load data into a byte buffer
        byte[] b = IOUtils.toByteArray(stream);
        ByteBuffer data = BufferUtils.createByteBuffer(b.length).put(b);
        data.flip();

        // load audio data into appropriate system space
        AL10.alBufferData(buffer.get(0), openAlFormat, data, (int) format.getSampleRate());
        // return the rough notion of length for the audio stream!
        return (long) (1000f * stream.getFrameLength() / format.getFrameRate());
    }

    public static Thread playSound(String file, float volume, float pitch, boolean looping) {
        return new Thread(() -> {
            int buffer = Audio.loadSound(file);

            source = new Source();
            source.setVolume(volume);
            source.setPitch(pitch);
            source.setLooping(looping);
            source.play(buffer);

            try {
                Thread.sleep(time);
            } catch (InterruptedException ignored) {
            }

            source.delete();
        });
    }

    public static long getTime() {
        return time;
    }

    public static Source getSource() {
        return source;
    }
}
