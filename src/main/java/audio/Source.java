package audio;

import org.lwjgl.openal.AL10;

/**
 * Created by ubihinon on 26.12.16.
 * This is source of sound
 */
public class Source {
    private int sourceId;

    public Source() {
        sourceId = AL10.alGenSources();
    }

    public void play(int buffer) {
        stop();
        AL10.alSourcei(sourceId, AL10.AL_BUFFER, buffer);
        continuePlaying();
    }

    public void stop() {
        AL10.alSourceStop(sourceId);
    }

    public void pause() {
        AL10.alSourcePause(sourceId);
    }

    public void continuePlaying() {
        AL10.alSourcePlay(sourceId);
    }

    public void delete() {
        stop();
        AL10.alDeleteSources(sourceId);
    }

    public void setVolume(float volume) {
        AL10.alSourcef(sourceId, AL10.AL_GAIN, volume);
    }

    public void setVelocity(float x, float y) {
        AL10.alSource3f(sourceId, AL10.AL_VELOCITY, x, y, 0f);
    }

    public void setPitch(float pitch) {
        AL10.alSourcef(sourceId, AL10.AL_PITCH, pitch);
    }

    public void setPosition(float x, float y) {
        AL10.alSource3f(sourceId, AL10.AL_POSITION, x, y, 0f);
    }

    public void setLooping(boolean looping) {
        AL10.alSourcei(sourceId, AL10.AL_LOOPING, looping ? AL10.AL_TRUE : AL10.AL_FALSE);
    }

    public boolean isPlaying() {
        return AL10.alGetSourcei(sourceId, AL10.AL_SOURCE_STATE) == AL10.AL_PLAYING;
    }
}
