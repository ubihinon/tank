import assets.Asset;
import audio.Audio;
import audio.Source;
import fontCreator.Text;
import fontCreator.TextCreator;
import io.Timer;
import io.UpdateLayer;
import io.Window;
import org.lwjgl.glfw.GLFWErrorCallback;
import org.lwjgl.opengl.GL;
import render.Camera;
import render.MainShader;
import world.TileRenderer;
import world.World;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;

/**
 * Created by ubihinon on 17.11.16.
 *
 */

public class Main {

    private Window window;
    private boolean showTechInfo = false;
    private World world;
    private Source menuSource;

    private void run() {
        try {
            init();
            loop();

            window.destroy();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            // Terminate GLFW and free the error callback
            glfwTerminate();
            glfwSetErrorCallback(null).free();
        }
    }

    private void init() {
        // Setup an error callback. The default implementation
        // will print the error message in System.err.
        GLFWErrorCallback.createPrint(System.err).set();

        // Initialize GLFW. Most GLFW functions will not work before doing this.
        if ( !glfwInit() )
            throw new IllegalStateException("Unable to initialize GLFW");

        // Configure our window
        glfwDefaultWindowHints(); // optional, the current window hints are already the default
        glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE); // the window will stay hidden after creation
        glfwWindowHint(GLFW_RESIZABLE, GLFW_FALSE); // the window will be resizable

        // Create the window
        window = new Window();
        window.setSize(750, 800);
        window.createWindow("Tanks");
    }

    private void loop() throws Exception {
        // This line is critical for LWJGL's interoperation with GLFW's
        // OpenGL context, or any context that is managed externally.
        // LWJGL detects the context that is current in the current thread,
        // creates the GLCapabilities instance and makes the OpenGL
        // bindings available for use.
        GL.createCapabilities();

        glEnable(GL_TEXTURE_2D);

        Camera camera = new Camera(window.getWidth(), window.getHeight());
        TileRenderer tiles = new TileRenderer();
        Asset.initAsset();

        MainShader mainShader = new MainShader();
        UpdateLayer updateLayer = new UpdateLayer(window);

        double frameCap = 1.0 / 60.0;

        double frameTime = 0;
        int frames = 0;
        double time = Timer.getTime();
        double unprocessed = 0;

        Text text = new Text(window);
        int fps = 0;

        Audio.init();
        Audio.setListenerData(0f,0f);

        final boolean[] playFailSound = {true};
        final boolean[] playWinSound = {true};
        final boolean[] playMenuSound = {true};

        TextCreator textCreator = new TextCreator();
        textCreator.init();

        // Run the rendering loop until the user has attempted to close
        // the window or has pressed the ESCAPE key.
        while (!window.shouldClose()) {
            boolean canRender = false;
            double time2 = Timer.getTime();
            double passed = time2 - time;
            unprocessed += passed;
            frameTime +=passed;

            time = time2;

            if(world == null) {
                if(playMenuSound[0]) {
                    playMenuSound[0] = false;
                    Audio.playSound("sounds/menu.wav", 1.0f, 1, true).start();
                }
                text.draw("TANKS GAME", 40, -300.0f, 0.0f);
                updateLayer.render();
                if(updateLayer.isUpdateButtonPressed()) {
                    createWorld(camera);
                    if(Audio.getSource().isPlaying()) {
                        Audio.getSource().delete();
                    }
                }
            }

            if(world != null) {
                world.update((float) frameCap, window, camera);
                world.correctCamera(camera, window);

                if(window.getInput().isKeyDown(GLFW_KEY_F1)) {
                    showTechInfo = !showTechInfo;
                }

                while(unprocessed >= frameCap) {
                    if(window.isHasResized()) {
                        camera.setProjection(window.getWidth(), window.getHeight());
                        world.calculateView(window);
                        glViewport(0, 0, window.getWidth(), window.getHeight());
                    }

                    unprocessed -= frameCap;
                    canRender = true;
                    if(frameTime >= 1.0) {
                        frameTime = 0;
                        fps = frames;
                        frames = 0;
                    }
                }
            }
            if(canRender) {
                glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // clear the framebuffer

                if(world.getKilledEnemies().get() >= world.getCOUNT_ENEMIES()) {
                    if(playWinSound[0]) {
                        playWinSound[0] = false;
                        playMenuSound("sounds/winning.wav");
                    }
                    text.draw("YOU WON!!!", 40, -300.0f, 0.0f);
                    updateLayer.render();
                    if(updateLayer.isUpdateButtonPressed()) {
                        createWorld(camera);
                        menuSource.delete();
                        playWinSound[0] = true;
                    }
                } else {
                    if(world.getTries() > 0) {
                        world.render(tiles, mainShader, camera);
                        if(showTechInfo) {
                            text.draw("FPS " + String.valueOf(fps), 5.0f,
                                    -window.getWidth() / 2 + 25,
                                    window.getHeight() / 2 - 60);
                        }
                    } else {
                        if(playFailSound[0]) {
                            playFailSound[0] = false;
                            playMenuSound("sounds/lose.wav");
                        }
                        text.draw("GAME OVER", 30, -window.getWidth() / 4, 0.0f);
                        updateLayer.render();
                        if(updateLayer.isUpdateButtonPressed()) {
                            createWorld(camera);
                            menuSource.delete();
                            playFailSound[0] = true;
                        }
                    }
                }
                frames++;
            }

            window.swapBuffers();
            // Poll for window events. The key callback above will only be
            // invoked during this call.
            window.update();
        }
        Asset.deleteAsset();
        Audio.cleanUp();
    }

    private void createWorld(Camera camera) {
        world = null;
        world = new World("level", camera);
        world.calculateView(window);
        world.setTries(2);
    }

    private void playMenuSound(String file) {
        new Thread(() -> {
            try {
                int buffer = Audio.loadSound(file);

                Source source = new Source();
                source.setVolume(1.0f);
                source.setPitch(1);
                float winYPos = 0;
                source.setPosition(0, winYPos);
                source.setLooping(false);
                source.play(buffer);

                Thread menuThread = new Thread(() -> {
                    try {
                        int menuBuffer = Audio.loadSound("sounds/menu.wav");

                        menuSource = new Source();
                        menuSource.setVolume(1.0f);
                        menuSource.setPitch(1);
                        float menuYPos = 10;
                        menuSource.setPosition(0, menuYPos);
                        menuSource.setLooping(true);


                        menuSource.play(menuBuffer);
                        while(menuSource.isPlaying()) {
                            if(menuYPos > 0) {
                                menuYPos -= 0.10;
                                menuSource.setPosition(0, menuYPos);
                            }
                            Thread.sleep(5);
                        }
                        menuSource.delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                });

                while(source.isPlaying()) {
                    winYPos += 0.10;
                    source.setPosition(0, winYPos);
                    menuThread.start();
                    Thread.sleep(5);
                }
                source.delete();
            } catch (Exception ignored) {
            }
        }).start();
    }

    public static void main(String[] args) {
        new Main().run();
    }
}