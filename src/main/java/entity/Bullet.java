package entity;

import collision.AABB;
import collision.Collision;
import io.Window;
import org.joml.Vector2f;
import render.Animation;
import render.Camera;
import world.World;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ubihinon on 06.12.16.
 *
 */
public class Bullet extends Entity {
    private static final int ANIM_SIZE = 5;
    private Timer timer;
    private boolean blowingUp = false;

    Bullet(Transform transform) {
        super(ANIM_SIZE, transform, 1.1f);
        setAnimation(TOP, new Animation(1, 0, "textures/tiles.png", 6, 0, 32));
        setAnimation(DOWN, new Animation(1, 0, "textures/tiles.png", 3, 0, 32));
        setAnimation(LEFT, new Animation(1, 0, "textures/tiles.png", 5, 0, 32));
        setAnimation(RIGHT, new Animation(1, 0, "textures/tiles.png", 4, 0, 32));
        setAnimation(EXPLOSION, new Animation(3, 24, "textures/tiles.png", 0, 0, 31));
    }

    @Override
    public void update(float delta, Window window, Camera camera, World world) {
        timer = new Timer();
        UpdateTask updateTask = new UpdateTask();
        updateTask.setDelta(delta);
        timer.schedule(updateTask, 0, 2);
    }

    private class UpdateTask extends TimerTask {
        private float delta;
        private int count;

        UpdateTask() {
            this.count = 0;
        }

        @Override
        public void run() {
            if(count++ >= 3000) {
                destroy();
                return;
            }
            Vector2f movement = new Vector2f();
            float step = 2 * delta;
            if(currentDirection == TOP) {
                movement.add(0, step);
                useAnimation(TOP);
            } else if(currentDirection == DOWN) {
                movement.add(0, -step);
                useAnimation(DOWN);
            } else if(currentDirection == LEFT) {
                movement.add(-step, 0);
                useAnimation(LEFT);
            } else if(currentDirection == RIGHT) {
                movement.add(step, 0);
                useAnimation(RIGHT);
            }
            move(movement);
        }

        void setDelta(float delta) {
            this.delta = delta;
        }
    }

    @Override
    protected void destroy() {
        useAnimation(EXPLOSION);
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                destroyed = true;
            }
        }, 100);
        timer.cancel();
    }

    @Override
    public void collideWithEntity(Entity entity) {
        Collision collision = boundingBox.getCollision(entity.boundingBox);
        if(collision.isIntersecting) {
            if(getUseAnimation() != EXPLOSION && !blowingUp) {
                blowingUp = true;
                entity.destroy();
                destroy();
            }
        }

    }

    @Override
    public void collideWithTiles(World world) {
        AABB[] boxes = new AABB[25];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                boxes[i + j * 5] = world.getTileBoundingBox(
                        (int) (((transform.pos.x / 2) + 0.5f) - (5 / 2)) + i,
                        (int) (((-transform.pos.y / 2) + 0.5f) - (5 / 2)) + j
                );
            }
        }

        AABB box = getBoxCollision(boxes, 0);

        if(box != null) {
            Collision data = boundingBox.getCollision(box);
            if(data.isIntersecting) {
                destroy();
            }

            data = boundingBox.getCollision(box);
            if(data.isIntersecting) {
                destroy();
            }
        }
    }

    void setCurrentDirection(int currentDirection) {
        this.currentDirection = currentDirection;
    }

    public boolean isBlowingUp() {
        return blowingUp;
    }

    public void setBlowingUp(boolean blowingUp) {
        this.blowingUp = blowingUp;
    }
}
