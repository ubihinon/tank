package entity;

import org.joml.Matrix4f;
import org.joml.Vector3f;

import java.io.*;

/**
 * Created by ubihinon on 22.11.16.
 *
 */
public class Transform implements Serializable {
    public Vector3f pos;
    public Vector3f scale;

    public Transform() {
        pos = new Vector3f();
        scale = new Vector3f();
    }

    public Matrix4f getProjection(Matrix4f target) {
        target.translate(pos);
        target.scale(scale);
        return target;
    }

    public Transform copy() {
        Transform copiedTransform = new Transform();
        try {
            ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(outputStream);

            objectOutputStream.writeObject(this);
            objectOutputStream.close();

            ByteArrayInputStream inputStream = new ByteArrayInputStream(outputStream.toByteArray());
            ObjectInputStream objectInputStream = new ObjectInputStream(inputStream);

            copiedTransform = (Transform) objectInputStream.readObject();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return copiedTransform;
    }
}