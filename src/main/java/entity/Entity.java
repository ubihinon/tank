package entity;

import assets.Asset;
import collision.AABB;
import collision.Collision;
import io.Window;
import org.joml.Matrix4f;
import org.joml.Vector2f;
import org.joml.Vector3f;
import render.Animation;
import render.Camera;
import render.MainShader;
import world.World;

import static org.lwjgl.opengl.GL11.*;

/**
 * Created by ubihinon on 25.11.16.
 *
 */
public abstract class Entity {
    private Animation[] animations;
    private int useAnimation;

    protected World world;

    static final int TOP        = 0;
    static final int DOWN       = 1;
    static final int LEFT       = 2;
    static final int RIGHT      = 3;
    static final int EXPLOSION  = 4;

    int currentDirection;
    boolean destroyed;
    AABB boundingBox;
    Transform transform;
    Camera camera;

    Entity(int maxAnimations, Transform transform, float collisionIndent) {
        animations = new Animation[maxAnimations];
        destroyed = false;

        this.transform = transform;
        this.useAnimation = 0;
        transform.scale = new Vector3f(1, 1, 0);

        boundingBox = new AABB(
                new Vector2f(transform.pos.x, transform.pos.y),
                new Vector2f(transform.scale.x - collisionIndent,transform.scale.y - collisionIndent)
        );
    }

    void setAnimation(int index, Animation animation) {
        animations[index] = animation;
    }

    Animation getAnimation(int index) {
        return animations[index];
    }

    void useAnimation(int index) {
        this.useAnimation = index;
    }

    int getUseAnimation() {
        return this.useAnimation;
    }

     void move(Vector2f direction) {
        transform.pos.add(new Vector3f(direction, 0));
        boundingBox.getCenter().set(transform.pos.x, transform.pos.y);
    }

    public void collideWithEntity(Entity entity) {
        Collision collision = boundingBox.getCollision(entity.boundingBox);

        if(collision.isIntersecting) {
            collision.distance.x /= 2;
            collision.distance.y /= 2;
            boundingBox.correctPosition(entity.boundingBox, collision);
            transform.pos.set(boundingBox.getCenter().x, boundingBox.getCenter().y, 0);

            entity.boundingBox.correctPosition(boundingBox, collision);
            entity.transform.pos.set(
                    entity.boundingBox.getCenter().x, entity.boundingBox.getCenter().y, 0
            );
        }
    }

    public void collideWithTiles(World world) {
        AABB[] boxes = new AABB[25];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                boxes[i + j * 5] = world.getTileBoundingBox(
                        (int) (((transform.pos.x / 2) + 0.5f) - (5 / 2)) + i,
                        (int) (((-transform.pos.y / 2) + 0.5f) - (5 / 2)) + j
                );
            }
        }

        AABB box = getBoxCollision(boxes, 0);
        boundingBox.setCurrentDirection(currentDirection);
        if(box != null) {
            Collision data = boundingBox.getCollision(box);
            if(data.isIntersecting) {
                boundingBox.correctPosition(box, data);
                transform.pos.set(boundingBox.getCenter(), 0);
            }
        }

        collideWithMapBounds();
    }

    protected AABB getBoxCollision(AABB[] boxes, float shortenBox) {
        AABB box = null;
        for (int i = 0; i < boxes.length; i++) {
            if(boxes[i] != null) {
                if(box == null) {
                    box = boxes[i];
                }

                Vector2f length1 = box.getCenter().sub(transform.pos.x - shortenBox,
                        transform.pos.y - shortenBox, new Vector2f());
                Vector2f length2 = boxes[i].getCenter().sub(transform.pos.x - shortenBox,
                        transform.pos.y - shortenBox, new Vector2f());

                if(length1.lengthSquared() > length2.lengthSquared()) {
                    box = boxes[i];
                }
            }
        }
        return box;
    }

    protected boolean collideWithMapBounds() {
        boolean collide = false;
        if(transform.pos.x > 28.2) {
            collide = true;
            transform.pos.set(transform.pos.x - 0.1f, transform.pos.y, 0.0f);
        } else if(transform.pos.x < -0.2f) {
            collide = true;
            transform.pos.set(transform.pos.x + 0.1f, transform.pos.y, 0.0f);
        } else if(transform.pos.y < -28.2) {
            collide = true;
            transform.pos.set(transform.pos.x, transform.pos.y + 0.1f, 0.0f);
        } else if(transform.pos.y > 0.2f) {
            collide = true;
            transform.pos.set(transform.pos.x, transform.pos.y - 0.1f, 0.0f);
        }
        return collide;
    }

    public abstract void update(float delta, Window window, Camera camera, World world);

    public void render(MainShader shader, Camera camera, World world) {
        glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
        glEnable(GL_BLEND);

        this.world = world;
        this.camera = camera;

        Matrix4f target = camera.getProjection();
        target.mul(world.getWorldMatrix());

        shader.bind();
        shader.setUniform("sampler", 0);
        shader.setUniform("projection", transform.getProjection(target));
        animations[useAnimation].bind(0);
        Asset.getModel().render();

        glDisable(GL_BLEND);
    }

    abstract protected void destroy();

    public boolean isDestroyed() {
        return destroyed;
    }
}
