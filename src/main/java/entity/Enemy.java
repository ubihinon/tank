package entity;

import collision.AABB;
import collision.Collision;
import io.Window;
import org.joml.Vector2f;
import render.Animation;
import render.Camera;
import world.World;

import java.util.concurrent.ThreadLocalRandom;

/**
 * Created by ubihinon on 05.12.16.
 *
 */
public class Enemy extends Tank implements Comparable {
    private static final int ANIM_SIZE = 5;

    public Enemy(Transform transform) {
        super(ANIM_SIZE, transform, 0.2f);
        setAnimation(TOP, new Animation(7, 7, "textures/tiles.png", 15, 9, 32));
        setAnimation(DOWN, new Animation(7, 7, "textures/tiles.png", 15, 10, 32));
        setAnimation(LEFT, new Animation(1, 7, 7, "textures/tiles.png", 20, 2, 32));
        setAnimation(RIGHT, new Animation(1, 7, 7, "textures/tiles.png", 21, 2, 32));
        currentDirection = ThreadLocalRandom.current().nextInt(2, 4);
        timeBetweenShoots = 1000;
    }

    @Override
    public int compareTo(Object o) {
        return 0;
    }

    private void changeDirection() {
        currentDirection = ThreadLocalRandom.current().nextInt(0, 4);
    }

    @Override
    public void update(float delta, Window window, Camera camera, World world) {
        if(getUseAnimation() == EXPLOSION) {
            return;
        }

        Vector2f movement = new Vector2f();
        float step = 5 * delta;
        if(currentDirection == TOP) {
            moveTop(movement, step);
        } else if(currentDirection == DOWN) {
            moveDown(movement, step);
        } else if(currentDirection == LEFT) {
            moveLeft(movement, step);
        } else if(currentDirection == RIGHT) {
            moveRight(movement, step);
        }

        shoot(delta, window);
        move(movement);
    }

    @Override
    public void collideWithEntity(Entity entity) {
        if(entity instanceof Enemy) {
            return;
        }
        super.collideWithEntity(entity);
    }

    @Override
    public void collideWithTiles(World world) {
        AABB[] boxes = new AABB[25];
        for (int i = 0; i < 5; i++) {
            for (int j = 0; j < 5; j++) {
                boxes[i + j * 5] = world.getTileBoundingBox(
                        (int) (((transform.pos.x / 2) + 0.5f) - (5 / 2)) + i,
                        (int) (((-transform.pos.y / 2) + 0.5f) - (5 / 2)) + j
                );
            }
        }

        AABB box = getBoxCollision(boxes, 0.5f);
        boundingBox.setCurrentDirection(currentDirection);
        if(box != null) {
            Collision data = boundingBox.getCollision(box);
            if(data.isIntersecting) {
                changeDirection();
                boundingBox.correctPosition(box, data);
                boundingBox.correctPosition(data);
                transform.pos.set(boundingBox.getCenter(), 0);
            }
        }

        if(collideWithMapBounds()) {
            changeDirection();
        }
    }
}
