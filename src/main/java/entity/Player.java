package entity;

import audio.Audio;
import io.Window;
import org.joml.Vector2f;
import org.joml.Vector3f;
import render.Animation;
import render.Camera;
import world.World;

import static org.lwjgl.glfw.GLFW.*;

/**
 * Created by ubihinon on 22.11.16.
 *
 */
public class Player extends Tank {
    private static final int ANIM_SIZE = 5;

    public Player(Transform transform) {
        super(ANIM_SIZE, transform, 0.1f);
        setAnimation(TOP, new Animation(7, 0, "textures/tiles.png", 15, 0, 32));
        setAnimation(DOWN, new Animation(7, 0, "textures/tiles.png", 15, 1, 32));
        setAnimation(LEFT, new Animation(1, 7, 0, "textures/tiles.png", 16, 2, 32));
        setAnimation(RIGHT, new Animation(1, 7, 0, "textures/tiles.png", 15, 2, 32));
        currentDirection = TOP;
        timeBetweenShoots = 1000;
    }

    @Override
    public void update(float delta, Window window, Camera camera, World world) {
        if(getUseAnimation() == EXPLOSION) {
            return;
        }
        Vector2f movement = new Vector2f();
        float step = 6 * delta;

        if(window.getInput().isKeyDown(GLFW_KEY_W)) {
            moveTop(movement, step);
            bindTextureWhenStayOrMove(movement, TOP, 14);
        } else if(window.getInput().isKeyDown(GLFW_KEY_S)) {
            moveDown(movement, step);
            bindTextureWhenStayOrMove(movement, DOWN, 14);
        } else if(window.getInput().isKeyDown(GLFW_KEY_A)) {
            moveLeft(movement, step);
            bindTextureWhenStayOrMove(movement, LEFT, 14);
        } else if(window.getInput().isKeyDown(GLFW_KEY_D)) {
            moveRight(movement, step);
            bindTextureWhenStayOrMove(movement, RIGHT, 14);
        }

        if(window.getInput().isKeyPressed(GLFW_KEY_SPACE)) {
            shoot(delta, window);
        }

        move(movement);
        camera.getPosition().lerp(transform.pos.mul(-world.getScale(), new Vector3f()), 0.05f);
    }

    @Override
    void shoot(float delta, Window window) {
        if(canShoot) {
            Audio.playSound("sounds/canoon.wav", 1.0f, 2, false).start();
            super.shoot(delta, window);
        }
    }

    private void bindTextureWhenStayOrMove(Vector2f movement, int animId, int moveFps) {
        bindTextureWhenMove(animId, moveFps);
        if(movement.x == 0 || movement.y == 0) {
            getAnimation(animId).setFps(0).bind();
        }
    }

    private void bindTextureWhenMove(int animId, int moveFps) {
        getAnimation(animId).setFps(moveFps).bind();
    }
}
