package entity;

import audio.Audio;
import io.Window;
import org.joml.Vector2f;
import render.Animation;
import render.Camera;
import render.MainShader;
import world.World;

import java.util.HashSet;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by ubihinon on 13.12.16.
 *
 */
public abstract class Tank extends Entity {
    private Set<Bullet> bullets;
    protected boolean canShoot = true;
    int timeBetweenShoots = 1000;

    Tank(int maxAnimations, Transform transform, float collisionIndent) {
        super(maxAnimations, transform, collisionIndent);
        setAnimation(EXPLOSION, new Animation(3, 9, "textures/tiles.png", 0, 0, 31));
        bullets = new HashSet<>();
    }

    void moveTop(Vector2f movement, float step) {
        movement.add(0, step);
        useAnimation(TOP);
        currentDirection = TOP;
    }

    void moveDown(Vector2f movement, float step) {
        movement.add(0, -step);
        useAnimation(DOWN);
        currentDirection = DOWN;
    }

    void moveLeft(Vector2f movement, float step) {
        movement.add(-step, 0);
        useAnimation(LEFT);
        currentDirection = LEFT;
    }

    void moveRight(Vector2f movement, float step) {
        movement.add(step, 0);
        useAnimation(RIGHT);
        currentDirection = RIGHT;
    }

    public Set<Bullet> getBullets() {
        return bullets;
    }

    @Override
    public void render(MainShader shader, Camera camera, World world) {
        try {
            if(!destroyed) {
                super.render(shader, camera, world);
            }
            bullets.forEach(bullet -> {
                bullet.render(shader, camera, world);
                if(bullet.isDestroyed()) {
                    bullets.remove(bullet);
                }
            });
        } catch (Exception ignored) {
        }
    }

    @Override
    protected void destroy() {
        useAnimation(EXPLOSION);
        Audio.playSound("sounds/explosion.wav", 1.0f, 2, false).start();
        new Timer().schedule(new TimerTask() {
            @Override
            public void run() {
                destroyed = true;
            }
        }, 300);
    }

    void shoot(float delta, Window window) {
        if(canShoot) {
            Transform bulletTransform = transform.copy();
            switch (currentDirection) {
                case TOP:
                    bulletTransform.pos.y += 1;
                    break;
                case DOWN:
                    bulletTransform.pos.y -= 1;
                    break;
                case LEFT:
                    bulletTransform.pos.x -= 1;
                    break;
                case RIGHT:
                    bulletTransform.pos.x += 1;
                    break;
            }

            Bullet bullet = new Bullet(bulletTransform);
            bullet.setCurrentDirection(currentDirection);
            bullet.update(delta, window, camera, world);
            bullets.add(bullet);

            canShoot = false;
            new Timer().schedule(new TimerTask() {
                @Override
                public void run() {
                    canShoot = true;
                }
            }, timeBetweenShoots);
        }
    }
}
